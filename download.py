import os # file crawling
from logger import get_logger # logging
import requests # GET requests for AWS S3 bucket
import zipfile # unzip downloaded data
import shutil # moving files
from tabulate import tabulate # pretty tables/lists

_logger = get_logger()
url = "XXX"
ROOT = './'

def download_data(download_dir, file_name):
    """
    Download a single file from the OrbitalAI data
    """
    # Start the download
    file_name = os.path.basename(file_name)
    _logger.info(f"Downloading {file_name}")
    r = requests.get(url + file_name)
    if r.ok: # If the request is successful
        _logger.debug(f"Writing {file_name}")
        with open(download_dir + file_name, "wb") as f:
            f.write(r.content)
    else: # If the request is not successful
        _logger.error(f"Error while downloading {file_name}")
        _logger.error(f"Status code: {str(r.status_code)}")

def aws_download_resolve(download_dir, label_file):
    """
    creates download directory, unzips and cleans data
    """
    if not os.path.isdir(download_dir):
        os.makedirs(download_dir)
        for file_name in label_file: # for all in the predefined respective label file
            download_data(download_dir, file_name[0])
            zip_name = os.path.basename(file_name[0])
            local_path = os.path.join(download_dir, zip_name)
            # unzip the file
            with zipfile.ZipFile(local_path, 'r') as zip_ref:
                _logger.debug(f'Extracting {file_name[0]}')
                zip_ref.extractall(download_dir)
            # delete the zip file after extraction
            os.remove(local_path)
            for filename in os.listdir(download_dir + 'tiff_folder'):
              _logger.debug(f'Cleaning up {filename}')
              if "PHISAT2-BANDS" not in filename: # only keep the images we work on
                  file_path = os.path.join(download_dir + 'tiff_folder', filename)
                  # check if it's a file and not a directory
                  if os.path.isfile(file_path):
                      os.remove(file_path)
              else:
                shutil.move(os.path.join(download_dir + 'tiff_folder', filename), download_dir + filename)
        os.rmdir(download_dir + 'tiff_folder')
    else:
        #!rm -rf test_data/
        _logger.error(f"Error: The directory '{download_dir}' already exists.")

if __name__ == "__main__":
    # we read the predefined labels for the train/test/validation split
    verbose = True # <---  set True for verbose output

    """
    with open(ROOT + 'labels/' + 'test_data_labels.txt', 'r') as f:
        test_labels = f.readlines()
    test_labels = [[url.strip()] for url in test_labels]
    _logger.info(f'Test [{len(test_labels)*256}]')
    if verbose:
        _logger.info(tabulate(sorted(test_labels)))
 
    # download data
    aws_download_resolve("testing/", test_labels)
    """
    """
    with open(ROOT + 'labels/' + 'validation_data_labels.txt', 'r') as f:
        val_labels = f.readlines()
    val_labels = [[url.strip()] for url in val_labels]
    _logger.info(f'Validation [{len(val_labels)*256}]')
    if verbose:
        _logger.info(tabulate(sorted(val_labels)))
    # download data
    aws_download_resolve("validation/", val_labels)
    """
    with open(ROOT + 'labels/' + 'train_data_labels22.txt', 'r') as f:
        train_labels = f.readlines()
    train_labels = [[url.strip()] for url in train_labels]
    _logger.info(f'Training [{len(train_labels)*256}]')
    if verbose:
        _logger.info(tabulate(sorted(train_labels)))
    # download data
    aws_download_resolve("training22/", train_labels)
