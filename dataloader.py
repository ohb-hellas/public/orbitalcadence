import rasterio # to open GeoTIFFs
from logger import get_logger # logging
import os # file crawling
import tensorflow as tf # tensorflow==1.15
import numpy as np

_logger = get_logger()
ROOT = './'

def create_dataloader(data_dir, BATCH_SIZE, mode='testing'):
    _logger.info(f'Creating dataloader for {mode}')
    subdir = mode
    # crawl path and create a dataset of file paths
    # REMOVING ROOT FOR NOW TO AVOID "././"
    file_pattern = os.path.join(data_dir, subdir, '*.tiff')
    #file_pattern = os.path.join(ROOT, data_dir, subdir, '*.tiff')
    _logger.info(f'Searching {file_pattern}')
    files_ds = tf.data.Dataset.list_files(file_pattern, shuffle=False)
    _logger.info(f'Found {files_ds} in {file_pattern}')

    # apply load and preprocess function
    #dataset_tf = files_ds.map(tf_load_and_preprocess_image)
    # this one filters NaNs
    #dataset_tf = files_ds.map(tf_load_and_preprocess_image).filter(filter_none_images)
    dataset_tf = files_ds.map(tf_load_and_preprocess_image).filter(lambda img, valid: valid).map(lambda img, valid: img)

    # Apply the data transformation function
    dataset_tf = dataset_tf.map(data_transform)

    # pre-load the next batch while the current batch is being used
    dataset_tf = dataset_tf.prefetch(tf.data.experimental.AUTOTUNE)

    # batching
    #test_dataset_tf = test_dataset_tf.batch(BATCH_SIZE)
    return dataset_tf

def load_and_preprocess_image(image_path):
    with rasterio.open(image_path.decode('utf-8'), 'r') as src:
        image = src.read()
        image = np.transpose(image, (1,2,0))
        image = image / image.max()
        image = image.astype(np.float32)
        # Check for NaN values in the image
        if np.isnan(image).any():
            _logger.warning(f"NaN values detected in image {image_path}, skipping.")
            #return None  # Use None to indicate that this image should be skipped
            return np.zeros_like(image), False  # Also return a flag indicating it's a dummy
    #return image
    return image, True  # True indicating this is a valid image

def tf_load_and_preprocess_image(image_path, transforms=None):
    image, is_valid = tf.py_func(load_and_preprocess_image, [image_path], [tf.float32, tf.bool])
    #image = tf.py_func(load_and_preprocess_image, [image_path], tf.float32)
    image.set_shape([None, None, None])  # Set the shape to allow None values
    #return image
    return image, is_valid

def filter_none_images(image):
    return image is not None

def data_transform(image):
    # convert numpy array to TensorFlow tensor
    image = tf.convert_to_tensor(image)
    return image

def compute_dataset_statistics(dataset):
    # Initialize the sum, sum of squares, and count to compute the mean and variance
    sum_of_elements = 0
    sum_of_squares = 0
    count_of_elements = 0

    with tf.Session() as sess:
        iterator = dataset.make_one_shot_iterator()
        next_element = iterator.get_next()
        try:
            while True:
                # Read a batch of images
                images = sess.run(next_element)
                # Update the running sum and sum of squares
                sum_of_elements += np.sum(images)
                sum_of_squares += np.sum(np.square(images))
                count_of_elements += np.prod(images.shape)
        except tf.errors.OutOfRangeError:
            pass

    # Compute the mean and variance using the running statistics
    mean_of_elements = sum_of_elements / count_of_elements
    mean_of_squares = sum_of_squares / count_of_elements
    variance_of_elements = mean_of_squares - (mean_of_elements ** 2)
    
    return mean_of_elements, variance_of_elements
