# orbitalCADENCE

orbitalCADENCE: Orbital Content-Aware Data comprEssioN for multispeCtral Environments.

## Pipeline

![orbitalCADENCE_pipeline](src/orbitalCADENCE_pipeline.png)

## License
orbitalCADENCE © 2023 by OHB Hellas is licensed under CC BY-NC-SA 4.0.