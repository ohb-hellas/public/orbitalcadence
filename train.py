import warnings

# Filter out FutureWarnings.
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings('ignore', category=FutureWarning, module='tensorflow')
warnings.filterwarnings('ignore', category=FutureWarning, module='tensorboard')

from model import *
import tensorflow as tf
import numpy as np
from logger import get_logger # logging
from tabulate import tabulate # pretty tables/lists
from dataloader import create_dataloader, compute_dataset_statistics # dataloader util functions
import os
from math import log10, sqrt 
from random_name_generator import get_name # model names

# model saving
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io

# for using CPU
#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Suppress TensorFlow logging (1: INFO, 2: WARNING, and 3: ERROR)
#tf.logging.set_verbosity(tf.logging.ERROR)  # Suppress TensorFlow logging (INFO and WARNING)
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)  # Suppress TensorFlow logging (INFO and WARNING)

gpus = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=5096)])

_logger = get_logger()

tf.reset_default_graph()

# Set hyper-parameters.
# for 32 we get OOM (out-of-memory)
BATCH_SIZE = 16

#num_training_updates = 50000
num_training_updates = 100000
num_validation_updates = 10

num_hiddens = 1024
num_residual_hiddens = 512
num_residual_layers = 2

# This value is not that important, usually 64 works.
# This will not change the capacity in the information-bottleneck.
embedding_dim = 64

# The higher this value, the higher the capacity in the information bottleneck.
num_embeddings = 1024

# commitment_cost should be set appropriately. It's often useful to try a couple
# of values. It mostly depends on the scale of the reconstruction cost
# (log p(x|z)). So if the reconstruction cost is 100x higher, the
# commitment_cost should also be multiplied with the same amount.
commitment_cost = 0.75

# Use EMA updates for the codebook (instead of the Adam optimizer).
# This typically converges faster, and makes the model less dependent on choice
# of the optimizer. In the VQ-VAE paper EMA updates were not used (but was
# developed afterwards). See Appendix of the paper for more details.
vq_use_ema = False

# This is only used for EMA updates.
decay = 0.99

learning_rate = 1e-5

dataloader = create_dataloader('.', BATCH_SIZE, mode='training')
_logger.info("Created training dataloader")
valid_dataloader = create_dataloader('.', BATCH_SIZE, mode='validation')
_logger.info("Created validation dataloader")

# calculate variance
_, data_variance = compute_dataset_statistics(dataloader)
_logger.info(f"Train data variance: {data_variance:.3f}")
_, val_data_variance = compute_dataset_statistics(valid_dataloader)
_logger.info(f"Validation data variance: {val_data_variance:.3f}")

# Data Loading.
train_dataset_iterator = (
    dataloader
    #.map(cast_and_normalise_images)
    #.shuffle(10000)
    .repeat(-1)  # repeat indefinitely
    .batch(BATCH_SIZE)).make_one_shot_iterator()
valid_dataset_iterator = (
    valid_dataloader
    #.map(cast_and_normalise_images)
    .shuffle(100)
    .repeat(-1)  # repeat indefinitely
    .batch(BATCH_SIZE)).make_one_shot_iterator()

train_dataset_batch = train_dataset_iterator.get_next()
valid_dataset_batch = valid_dataset_iterator.get_next()

def get_images(sess, subset='train'):
  if subset == 'train':
    return sess.run(train_dataset_batch)
  elif subset == 'validation':
    return sess.run(valid_dataset_batch)

_logger.info("Building model")
# Build modules.
encoder = Encoder(num_hiddens, num_residual_layers, num_residual_hiddens)
decoder = Decoder(num_hiddens, num_residual_layers, num_residual_hiddens)
pre_vq_conv1 = snt.Conv2D(output_channels=embedding_dim,
    kernel_shape=(1, 1),
    stride=(1, 1),
    name="to_vq")

if vq_use_ema:
  vq_vae = snt.nets.VectorQuantizerEMA(
      embedding_dim=embedding_dim,
      num_embeddings=num_embeddings,
      commitment_cost=commitment_cost,
      decay=decay)
else:
  vq_vae = snt.nets.VectorQuantizer(
      embedding_dim=embedding_dim,
      num_embeddings=num_embeddings,
      commitment_cost=commitment_cost)

# Process inputs with conv stack, finishing with 1x1 to get to correct size.
x = tf.placeholder(tf.float32, shape=(None, 256, 256, 8))
z = pre_vq_conv1(encoder(x))

# vq_output_train["quantize"] are the quantized outputs of the encoder.
# That is also what is used during training with the straight-through estimator. 
# To get the one-hot coded assignments use vq_output_train["encodings"] instead.
# These encodings will not pass gradients into to encoder, 
# but can be used to train a PixelCNN on top afterwards.

def log10(x):
  numerator = tf.log(x)
  denominator = tf.log(tf.constant(10, dtype=numerator.dtype))
  return numerator / denominator

# image metrics
def compute_nmse(original, reconstructed):
    nmse = tf.reduce_mean((reconstructed - original)**2) / data_variance  # Normalized MSE
    return nmse

def compute_psnr(original, reconstructed): 
    mse = tf.reduce_mean(tf.squared_difference(original, reconstructed))
    psnr = tf.constant(20, dtype=mse.dtype) * log10(1.0 / tf.sqrt(mse))
    return psnr 

# For training
vq_output_train = vq_vae(z, is_training=True)
x_recon = decoder(vq_output_train["quantize"])
recon_error = tf.reduce_mean((x_recon - x)**2) / data_variance  # Normalized MSE
loss = recon_error + vq_output_train["loss"]
nmse = compute_nmse(x, x_recon)
psnr = compute_psnr(x, x_recon)

# For evaluation, make sure is_training=False!
vq_output_eval = vq_vae(z, is_training=False)
x_recon_eval = decoder(vq_output_eval["quantize"])
# CAUTIONNNNNNNNNNNNNN data_variance is from training dataset
val_recon_error = tf.reduce_mean((x_recon_eval - x)**2) / val_data_variance  # Normalized MSE for validation

# The following is a useful value to track during training.
# It indicates how many codes are 'active' on average.
perplexity = vq_output_train["perplexity"] 

val_recon_error_placeholder = tf.placeholder(tf.float32, shape=[], name='val_recon_error_placeholder')
val_perplexity_placeholder= tf.placeholder(tf.float32, shape=[], name='val_perplexity_placeholder')

# tensorboard
with tf.name_scope('summaries'):
    tf.summary.scalar('recon_error', recon_error)
    tf.summary.scalar('perplexity', perplexity)
    tf.summary.scalar('NMSE', nmse)
    tf.summary.scalar('PSNR', psnr)
    #tf.summary.scalar('val_recon_error', recon_error)
    #tf.summary.scalar('val_perplexity', perplexity)
    #tf.summary.scalar('val_NMSE', nmse)
    #tf.summary.scalar('val_PSNR', psnr)

with tf.name_scope('image_summaries'):
    #tmp1 = tf.concat([x[..., 2:3], x[..., 1:2], x[..., 0:1]], axis=-1)
    tf.summary.image('original_images', x[..., 0:1], max_outputs=4)
    tf.summary.image('reconstructed_images', x_recon[..., 0:1], max_outputs=4)

merged_summary_op = tf.summary.merge_all()

with tf.name_scope('validation_summaries'):
    validation_summary_op = tf.summary.merge([
        tf.summary.scalar('val_recon_error', val_recon_error_placeholder),
        tf.summary.scalar('val_perplexity', val_perplexity_placeholder)
    ])

# model saving
# Define the name of the output node
output_node_name = 'Decoder/output_node'

# We need to specify the name for our output node
x_recon = tf.identity(x_recon, name=output_node_name)

# GPU out-of-memory issues
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
config = tf.ConfigProto(gpu_options=gpu_options)
config.gpu_options.allow_growth = True # allows TensorFlow to allocate memory gradually instead of grabbing all available memory upfront

# Create optimizer and TF session.
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss)
sess = tf.train.SingularMonitoredSession(config=config)
run_name = f"{get_name(sep='_')}_iter{num_training_updates}_b{BATCH_SIZE}"
_logger.info(f"Run name: {run_name}")
summary_writer = tf.summary.FileWriter(f'./logs/{run_name}', sess.graph)

def validate_model(sess):
    avg_val_recon_error = []
    val_perplexity = []
    for i in range(num_validation_updates):
        #feed_dict = {x: get_images(sess, subset='train')}
        feed_dict = {x: get_images(sess, subset='validation')}
        #_logger.info(feed_dict)
        #_logger.info(f'******************************{np.isnan(np.sum(feed_dict))}')
        results = sess.run([val_recon_error], feed_dict=feed_dict)
        _logger.info(f'validation single iter recon_error:: {results[0]:.3f}')
        avg_val_recon_error.append(results[0])
        #val_perplexity.append(results[1])
    _logger.info(f'val_recon_error: {np.mean(avg_val_recon_error):.3f}')
    return np.mean(avg_val_recon_error)#, np.mean(val_perplexity)

# Train.
train_res_recon_error = []
train_res_perplexity = []

_logger.info("Starting training")
_logger.info("Calculating the answer to life, the universe and everything")
# maybe xrange?????
for i in range(num_training_updates):
    _logger.debug(f'run {run_name}, iter {i+1}')
    feed_dict = {x: get_images(sess, subset='train')}
    """
    results = sess.run([train_op, recon_error, perplexity],
					 feed_dict=feed_dict)
    train_res_recon_error.append(results[1])
    train_res_perplexity.append(results[2])
    """
    results = sess.run([train_op, recon_error, perplexity],
					 feed_dict=feed_dict)
    if (i+1) % 10 == 0:
        nmse_value = sess.run(nmse, feed_dict=feed_dict)
        psnr_value = sess.run(psnr, feed_dict=feed_dict)
        #summary_writer.add_summary([results[1], results[2]], i)
        manual_summary = tf.Summary()
        manual_summary.value.add(tag='recon_error', simple_value=results[1])
        manual_summary.value.add(tag='perplexity', simple_value=results[2])
        manual_summary.value.add(tag='NMSE', simple_value=nmse_value)
        manual_summary.value.add(tag='PSNR', simple_value=psnr_value)
        # For training summaries
        summary_str = sess.run(merged_summary_op, feed_dict=feed_dict)
        summary_writer.add_summary(summary_str, i+1)
        """
        val_recon_error, val_perplexity = validate_model(sess)
        manual_summary.value.add(tag='val_recon_error', simple_value=val_recon_error)
        manual_summary.value.add(tag='val_perplexity', simple_value=val_perplexity)
        """	
        train_res_recon_error.append(results[1])
        train_res_perplexity.append(results[2])
        rr = np.mean(train_res_recon_error[-10:])
        pp = np.mean(train_res_perplexity[-10:])
        _logger.info(f'iter: {i+1}, recon_error: {np.mean(train_res_recon_error[-10:]):.3f}, perplexity: {np.mean(train_res_perplexity[-10:]):.3f}')
        #_logger.info(f"x shape: {x.shape}, x_recon shape: {x_recon.shape}")

        """
        feed_dict = {x: get_images(sess, subset='validation')}
        val_recon_error, val_perplexity = sess.run(
            [recon_error, perplexity], feed_dict=feed_dict)
        """
        #val_feed_dict = {
        #    val_recon_error_placeholder: val_recon_error,
        #    val_perplexity_placeholder: val_perplexity
        #}
        #manual_summary.value.add(tag='val_recon_error', simple_value=results[0])
        #manual_summary.value.add(tag='val_perplexity', simple_value=results[1])
        #manual_summary.value.add(tag='val_NMSE', simple_value=nmse_value)
        #manual_summary.value.add(tag='val_PSNR', simple_value=psnr_value)
        #summary_writer.add_summary(manual_summary, i+1)
        # For validation summaries
        #val_summary_str = sess.run(validation_summary_op, feed_dict=val_feed_dict)
        #summary_writer.add_summary(val_summary_str, i+1)
    if (i+1) % 100 == 0:
        avg_val_recon_error = validate_model(sess)
        val_summary = tf.Summary(value=[tf.Summary.Value(tag="val_recon_error", simple_value=avg_val_recon_error)])
        summary_writer.add_summary(val_summary, i + 1)

summary_writer.close()

# After training is complete, freeze the graph
output_graph_def = graph_util.convert_variables_to_constants(
    sess,
    sess.graph.as_graph_def(),
    [output_node_name]  # Here we specify the output node's name
    #[output_node_name.split('/')[0]]  # Here we specify the output node's name
)

# Then, we serialize and dump the output graph to the filesystem
_logger.info("Attempting to save model")
with tf.gfile.GFile(f'./model_{run_name}.pb', 'wb') as f:
    f.write(output_graph_def.SerializeToString())
_logger.info(f'Model model_{run_name} saved as frozen graph')
_logger.info(f"Answer to life: {42}")
