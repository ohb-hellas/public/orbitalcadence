# OrbitalCADENCE Converter

## Introduction

This folder contains a docker image that will convert a Neural Network into the Ubotica Neural Network format.

The image currently supports models developed with TensorFlow 1.x and saved in a frozen graph format (\*.pb).

The image outputs the model in UNN format (\*.unn), in the same folder and with the same name as the original model.

The conversion process follows this chain:

    TensorFlow Frozen Graph (*.pb)
                ↓
    IR format (*.xml, *.bin, *.mapping)
                ↓
    Ubotica Neural Network (*.unn)


## Usage

1. cd into the folder that contains the model,
2. Run the docker container:

`docker run -it -v $PWD:/app uphilld/orbitalcadence:converter <model filename>`

The image will then proceed to convert your model, first into the IR and then into the Ubotica Neural Network format.

For `<model filename>` use the filename without the path, e.g. `model.pb`.

## Files

This folder contains the following files:

1. build.sh → A script to build the image according to the recipe,
1. Dockerfile → The recipe for the image,
1. entrypoint.sh → The script that runs when the docker image is run,
2. README.md → This readme.
