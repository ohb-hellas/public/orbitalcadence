#!/bin/bash
echo -e "This script file builds and pushes the OrbitalCADENCE Converter image."
docker build -t uphilld/orbitalcadence:converter .
docker push uphilld/orbitalcadence:converter
