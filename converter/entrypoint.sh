#!/bin/bash
# Source environment variables
source /opt/intel/openvino_2020.3.355/bin/setupvars.sh -pyver 3.6

# Check for input
if [ $# -gt 0 ] && [[ "$1" == *.pb ]]; then
	
	model_file="$1"
	model_name="${model_file%.pb}"

	echo -e "Model appears to be in proper format, proceeding with conversion..."
	
	echo -e "Converting model to IR..."
	python3 /opt/intel/openvino_2020.3.355/deployment_tools/model_optimizer/mo_tf.py \
		--input_model /app/${model_file} \
		--output_dir /app/IR \
		--input_shape "[1, 256, 256, 8]" \
		--progress
	if [ -f /app/IR/${model_name}.xml ]; then
		echo -e "Model successfully converted to IR."
	else
		echo -e "The conversion to IR was unsuccessful."
		echo -e "Quitting..."
		exit -1
	fi

	echo -e "Converting model to Ubotica Rupia format..."
	/opt/intel/openvino_2020.3.355/deployment_tools/inference_engine/lib/intel64/myriad_compile \
		-m /app/IR/${model_name}.xml \
		-VPU_MYRIAD_PLATFORM VPU_MYRIAD_2450 \
		-VPU_NUMBER_OF_SHAVES 8 \
		-VPU_NUMBER_OF_CMX_SLICES 8 \
		-o /app/${model_name}.unn
	
	if [ -f /app/${model_name}.unn ]; then
		echo -e "Model successfully converted to Ubotica Rupia format."
		echo -e "Deleting leftovers..."
		rm -rf /app/IR/
		exit 0
	else
		echo -e "The conversion to UNN was unsuccessful."
		echo -e "Quitting..."
		exit -1
	fi
else
	# Open bash prompt
	echo -e "No valid arguments detected, launching bash..."
	exec bash
fi
